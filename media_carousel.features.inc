<?php
/**
 * @file
 * media_carousel.features.inc
 */

/**
 * Implementation of hook_ctools_plugin_api().
 */
function media_carousel_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "file_entity" && $api == "file_default_displays") {
    return array("version" => "1");
  }
}

/**
 * Implementation of hook_views_api().
 */
function media_carousel_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}

/**
 * Implementation of hook_image_default_styles().
 */
function media_carousel_image_default_styles() {
  $styles = array();

  // Exported image style: media_carousel
  $styles['media_carousel'] = array(
    'name' => 'media_carousel',
    'effects' => array(
      1 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => '530',
          'height' => '290',
        ),
        'weight' => '1',
      ),
    ),
  );

  return $styles;
}

/**
 * Implementation of hook_node_info().
 */
function media_carousel_node_info() {
  $items = array(
    'featured_item' => array(
      'name' => t('Featured Item'),
      'base' => 'node_content',
      'description' => t('Adds an item to the Featured Items carousel'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
