<?php

/**
 * @file media_carousel/media_carousel.styles.inc
 * Styles definitions for the Media Carousel
**/

/**
 * Implementation of Styles module hook_styles_default_containers()
**/

// function media_carousel_styles_default_containers() {
//   return array(
//     'file' => array(
//       'containers' => array(
//         'class' => 'MediaCarouselStyles',
//         'name' => 'media_carousel',
//         'label' => t('Media Carousel'),
//         'preview' => 'media_carousel_preview_style',
//       ),
//     ),
//   );
// }

/**
 * Implementation of Styles module hook_styles_default_presets()
**/

// function media_carousel_styles_default_presets() {
//   $presets = array(
//     'file' => array(
//       'containers' => array(
//         'media_flickr' => array(
//           'presets' => array(
//             'media_carousel_default' => array(
//               'name' => 'media_carousel_default'
//               'settings' => array(
//                 'height' => 290,
//                 'width' => 530,
//               ),
//             ),
//           ),
//         ),
//       ),
//     ),
//   );
//   dsm('Hello World');
//   return $presets;
// }