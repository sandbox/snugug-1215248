<?php
/**
 * @file
 * media_carousel.views_default.inc
 */

/**
 * Implementation of hook_views_default_views().
 */
function media_carousel_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'media_carousel';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Media Carousel';
  $view->core = 7;
  $view->api_version = '3.0-alpha1';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Featured URL */
  $handler->display->display_options['fields']['field_featured_url']['id'] = 'field_featured_url';
  $handler->display->display_options['fields']['field_featured_url']['table'] = 'field_data_field_featured_url';
  $handler->display->display_options['fields']['field_featured_url']['field'] = 'field_featured_url';
  $handler->display->display_options['fields']['field_featured_url']['label'] = '';
  $handler->display->display_options['fields']['field_featured_url']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_featured_url']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_featured_url']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_featured_url']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_featured_url']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_featured_url']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_featured_url']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_featured_url']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_featured_url']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_featured_url']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_featured_url']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_featured_url']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_featured_url']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_featured_url']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_featured_url']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_featured_url']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_featured_url']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_featured_url']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['field_featured_url']['field_api_classes'] = 0;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 1;
  $handler->display->display_options['fields']['title']['alter']['path'] = '[field_featured_url]';
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 1;
  $handler->display->display_options['fields']['title']['alter']['external'] = 0;
  $handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['title']['link_to_node'] = 0;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['body']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['body']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['body']['alter']['external'] = 0;
  $handler->display->display_options['fields']['body']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['body']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['body']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['body']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['body']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['body']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['body']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['body']['alter']['html'] = 0;
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['body']['hide_empty'] = 0;
  $handler->display->display_options['fields']['body']['empty_zero'] = 0;
  $handler->display->display_options['fields']['body']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['body']['type'] = 'text_plain';
  $handler->display->display_options['fields']['body']['field_api_classes'] = 0;
  /* Field: Content: Featured Media */
  $handler->display->display_options['fields']['field_featured_media']['id'] = 'field_featured_media';
  $handler->display->display_options['fields']['field_featured_media']['table'] = 'field_data_field_featured_media';
  $handler->display->display_options['fields']['field_featured_media']['field'] = 'field_featured_media';
  $handler->display->display_options['fields']['field_featured_media']['label'] = '';
  $handler->display->display_options['fields']['field_featured_media']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_featured_media']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_featured_media']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_featured_media']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_featured_media']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_featured_media']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_featured_media']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_featured_media']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_featured_media']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_featured_media']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_featured_media']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_featured_media']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_featured_media']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_featured_media']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_featured_media']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_featured_media']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_featured_media']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['field_featured_media']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_featured_media']['type'] = 'media';
  $handler->display->display_options['fields']['field_featured_media']['settings'] = array(
    'file_view_mode' => 'media_carousel',
  );
  $handler->display->display_options['fields']['field_featured_media']['field_api_classes'] = 0;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 0;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'featured_item' => 'featured_item',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $export['media_carousel'] = $view;

  return $export;
}
