<?php
/**
 * @file
 * media_carousel.ds.inc
 */

/**
 * Implementation of hook_ds_view_modes_info().
 */
function media_carousel_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass;
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'media_carousel';
  $ds_view_mode->label = 'Media Carousel';
  $ds_view_mode->entities = array(
    'file' => 'file',
  );
  $export['media_carousel'] = $ds_view_mode;

  return $export;
}
