jQuery(function($) {
  var $ = jQuery;
  
  
  // Creates the Carousel
  $('#media-carousel').cycle({
    fx: 'fade',
    speed: 600,
    timeout: 4000,
    cleartypeNoBg: 1,
    pause: true,
    pauseOnPagerHover: 1,
    pager: '.media-carousel-nav'
  });
      
  // Binds touch events to the carousel to swipe back/forward
  $('#media-carousel').swipe({
    data: "#media-carousel",
    swipeLeft: function() {
      $('#media-carousel').cycle('next');
    },
    swipeRight: function() {
      $('#media-carousel').cycle('prev');
    },
    allowPageScroll: "vertical",
  });
  
  // Pauses carousel on YouTube play
  $('#media-carousel .youtube-player').each(function() {
    var youtube_id = $(this).attr('id');
    onYouTubePlayerReady(youtube_id);
    // alert(youtube_id);
  });
  
  function onYouTubePlayerReady(playerId) {
    ytplayer = document.getElementById(playerId);
    // ytplayer.addEventListener("onStateChange", "onytplayerStateChange");
  }

  function onytplayerStateChange(newState) {
    if (newState >= 1) {
      $('#media-carousel').cycle('pause');
    } else {
      $('#media-carousel').cycle('resume');
    }
  }
  
  // Resizing Functions
  if( $('#media-carousel').parent().width() < 940) {
    media_carousel_medium_layout();
    if ( $('#media-carousel').parent().width() < 570 ) {
      media_carousel_small_layout();
    }
  }
  
  $(window).resize(function() {
    var parentWidth = $('#media-carousel').parent().width();
    if( parentWidth < 940 && parentWidth >= 570) {
      media_carousel_medium_layout();
    } else if ( parentWidth < 570) {
      media_carousel_small_layout()
    } else {
      media_carousel_default_layout();
    }
  });
  
  function media_carousel_small_layout() {
    var parentWidth = $('#media-carousel').parent().width();
    // Resize Media
    var mediaItem = $('.styles.file-styles.media_carousel').children(":first-child")
    var originalWidth = mediaItem.width();
    var originalHeight = mediaItem.height();
    var newWidth = parentWidth - 20;
    var newHeight = newWidth / 530 * 290;
    mediaItem.width(newWidth);
    mediaItem.height(newHeight);
    // For YouTube embeds:
    $('#media-carousel .media-youtube-outer-wrapper, #media-carousel .media-youtube-preview-wrapper, #media-carousel .youtube-player').width(newWidth);
    $('#media-carousel .media-youtube-outer-wrapper, #media-carousel .media-youtube-preview-wrapper, #media-carousel .youtube-player').height(newHeight);
    
    // Resize and Reposition Main Carousel
    $('#media-carousel').width(parentWidth);
    $('#media-carousel').height(newHeight+30);
    $('#media-carousel').css('margin-left', -(parentWidth / 2)+'px');
    
    // Hide Description
    $('.media-carousel-description').css('display', 'none');
    
    // Resize and Reposition Title
    $('.media-carousel-title').width(newWidth - 20);
    // $('.media-carousel-title').css('overflow', 'hidden');
    $('.media-carousel-title h2').css('font-size', '15px');
    $('.media-carousel-title h2').css('line-height', '20px');
    $('.media-carousel-title h2').css('margin-top', '-15px');
    $('.media-carousel-title h2').css('text-align', 'left');
    $('.media-carousel-text-block').width(newWidth);
    $('.media-carousel-text-block').height(30);
    $('.media-carousel-text-block').css('position', 'absolute');
    // $('.media-carousel-text-block').css('background', '#000');
    // $('.media-carousel-text-block').css('background-color', 'rgba(0, 27, 46, .5)');
    // $('.media-carousel-text-block').css('border', 'none');
    $('.media-carousel-text-block').css('margin-left', '1px');
    $('.media-carousel-text-block').css('margin-top', newHeight - 29+'px');
    
    // Move Media
    $('.media-carousel-media-block').css('float', 'left');
    $('.media-carousel-media-block').css('margin-left', "0px");
    $('.media-carousel-media-block').width(newWidth);
    $('.media-carousel-media-block').height(newHeight);
    
    
    // Move Nav
    $('.media-carousel-nav').css('margin-top', '0px');
    $('.media-carousel-nav').css('margin-left', (parentWidth - 145) + 'px');
    
    
    
    // alert('Messed');
  }
  
  
  function media_carousel_default_layout() {
    // Resize and Reposition Main Carousel
    $('#media-carousel').width(940);
    $('#media-carousel').css('margin-left', '-470px');
    
    // Hide Description
    $('.media-carousel-description').css('display', 'block');
    
    // Resize and Reposition Title
    $('.media-carousel-title').width(288.996944233);
    $('.media-carousel-title h2').css('font-size', '28px');
    $('.media-carousel-title h2').css('line-height', '33px');
    $('.media-carousel-title h2').css('margin-top', '0px');
    // $('.media-carousel-title h2').css('text-align', 'center');
    $('.media-carousel-text-block').width(370);
    $('.media-carousel-text-block').height(291);
    $('.media-carousel-text-block').css('position', 'relative');
    // $('.media-carousel-text-block').css('background', '#001b2e');
    // $('.media-carousel-text-block').css('background-color', '-webkit-gradient(radial, 185 -150, 300, 185 -50, 75, from(#001B2E), to(#0071A5))');
    // $('.media-carousel-text-block').css('border', '1px solid #0071a5');
    $('.media-carousel-text-block').css('margin-left', '0px');
    $('.media-carousel-text-block').css('margin-top', '0px');
    
    // Move Media
    $('.media-carousel-media-block').css('float', 'right');
    $('.media-carousel-media-block').css('margin-left', "0px");
    
    // Move Nav
    $('.media-carousel-nav').css('margin-top', '245px');
    $('.media-carousel-nav').css('margin-left', '128px');
    
  }
  
  function media_carousel_medium_layout() {
    // Resize and Reposition Main Carousel
    $('#media-carousel').width(570);
    $('#media-carousel').css('margin-left', -(570 / 2)+'px');
    
    // Hide Description
    $('.media-carousel-description').css('display', 'none');
    
    // Resize and Reposition Title
    $('.media-carousel-title').width(510);
    $('.media-carousel-title h2').css('font-size', '20px');
    $('.media-carousel-title h2').css('line-height', '20px');
    $('.media-carousel-title h2').css('margin-top', '-15px');
    $('.media-carousel-title h2').css('text-align', 'left');
    $('.media-carousel-text-block').width(530);
    $('.media-carousel-text-block').height(30);
    $('.media-carousel-text-block').css('position', 'absolute');
    // $('.media-carousel-text-block').css('background', '#000');
    // $('.media-carousel-text-block').css('background-color', 'rgba(0, 27, 46, .5)');
    // $('.media-carousel-text-block').css('border', 'none');
    $('.media-carousel-text-block').css('margin-left', '11px');
    $('.media-carousel-text-block').css('margin-top', '261px');
    
    
    // Move Media
    $('.media-carousel-media-block').css('float', 'left');
    $('.media-carousel-media-block').css('margin-left', (550 - 530) / 2+"px");
    
    // Move Nav
    $('.media-carousel-nav').css('margin-top', '0px');
    $('.media-carousel-nav').css('margin-left', '425px');
    // alert('Messed');
  }
   
});