<?php
/**
 * @file views-view-unformatted--media-carousel.tpl.php
 * Default simple view template to display the media carousel
 *
 * @ingroup views_templates
 */
?>

<div id="media-carousel" class="clearfix">
  <?php foreach ($rows as $id => $row): ?>
    <figure class="media-carousel-slide clearfix">
      <?php print $row; ?>
    </figure>
  <?php endforeach; ?>
</div>