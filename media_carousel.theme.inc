<?php
/*
 * implimentation of hook_theme()
 *
 * We are targeting view templates that cross many views
 */
function media_carousel_theme($existing, $type, $theme, $path) {
  $themes = array();
  
  // we want to intersept all page and block rows and point them at the same
  // file we are going to uses views-view-fields--page
  // — Row-level theme for stories
  $key = 'views_view_fields__media_carousel__block';
  $tpl_path = 'themes/' . str_replace('_', '-', $key); 
  $themes[$key] = array (
    'arguments' => array('view' => Null, 'options' => Null, 'row' => Null),
    'template' => $tpl_path,
    'original hook' => 'views_view_fields',
    
    // — According to Views Advanced Help, we either need to do this or make module weight > 10.
       'preprocess functions' => array(
             'template_preprocess',
             'template_preprocess_views_view_fields',
             "media_carousel_preprocess_$key",
       ),
     );
     //display
     $key = 'views_view__media_carousel__block';
     $tpl_path = 'themes/' . str_replace('_', '-', $key); 
     $themes[$key] = array (
       'arguments' => array('view' => Null),
       'template' => $tpl_path,
       'original hook' => 'views_view',
       
       // — According to Views Advanced Help, we either need to do this or make module weight > 10.
       'preprocess functions' => array(
             'template_preprocess',
             'template_preprocess_views_view',
             "media_carousel_preprocess_$key",
       ),
     );
     //style
     $key = 'views_view_unformatted__media_carousel__block';
     $tpl_path = 'themes/' . str_replace('_', '-', $key); 
     $themes[$key] = array (
       'arguments' => array('view' => Null, 'options' => Null, 'rows' => Null, 'title' => Null),
       'template' => $tpl_path,
       'original hook' => 'views_view_unformatted',
       
       // — According to Views Advanced Help, we either need to do this or make module weight > 10.
       'preprocess functions' => array(
             'template_preprocess',
             'template_preprocess_views_view_unformatted',
             "media_carousel_preprocess_$key",
       ),
     );
  return $themes;
}

/**
 *  Preprocess function for home page feature rotator fields
 */
function media_carousel_preprocess_views_view_fields__media_carousel__block(&$vars) {
  drupal_add_js(drupal_get_path("module", 'media_carousel') . "/themes/js/jquery.cycle.all.js", 'file');
  drupal_add_js(drupal_get_path("module", 'media_carousel') . "/themes/js/jquery.jswipe-0.1.2.js", 'file');
  drupal_add_js(drupal_get_path("module", 'media_carousel') . "/themes/js/media-carousel.js", 'file');
  drupal_add_css(drupal_get_path("module", 'media_carousel') . "/themes/css/media-carousel.css", 'file');
}
